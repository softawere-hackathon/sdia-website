Please make sure you check the [Notion page](https://sdia.notion.site/SDIA-Website-v2-3357b08e6a124c9397f546910550dfcf) for an overview of technical information.

## 🚀 Installation

1.  **Install yarn & packages**

    Use Yarn to install packages.

    ```shell
    yarn install
    ```

2.  **Start developing.**

    Navigate into your new site’s directory and start it up.

    ```shell
    cd my-gatsby-site/
    yarn run develop
    ```

3.  **Open the code and start customizing!**

    Your site is now running at http://localhost:8000!
