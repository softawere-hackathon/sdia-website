## Must have

* Enable previews right from the start
* Create a slices component that can render any slice dynamically
* Use the fragments in each slice
* Make a dynamic page
* Data Types: Organization Member, Team Member, Article, Initiative, Event, Roadmap Activity, Roadmap Steering Group
* Can Prismic just list all available tags? What are tags used for?
* Gatsby Pagination?
* Should there be a component/slice with just 'all blog posts' and 'all team members'
* Same for members, a slice that shows all members vs. featured members? Members by category? And then use static query to load them
* Look at HubSpot forms/as data source
