const path = require(`path`)


const convertStringToSnakeCase = str => str && str
  .match(/[A-Z]{2,}(?=[A-Z][a-z]+[0-9]*|\b)|[A-Z]?[a-z]+[0-9]*|[A-Z]|[0-9]+/g)
  .map(x => x.toLowerCase())
  .join('_');


exports.createPages = ({ graphql, actions }) => {
  const { createPage } = actions
  const roadmapActivityTemplate = path.resolve(`src/templates/roadmap-activity.js`)
  // Query for markdown nodes to use in creating pages.
  // You can query for whatever data you want to create pages for e.g.
  // products, portfolio items, landing pages, etc.
  // Variables can be added as the second function parameter
  return graphql(`
    query loadRoadmapActivitiesAndBlogPosts {
      allPrismicRoadmapActivity {
        edges {
          node {
            id
            uid
            url
            data {
              name {
                text
              }
              metric {
                document {
                  ... on PrismicRoadmapMetric {
                    uid
                    id
                  }
                }
              }
            }
          }
        }
      }
      allPrismicArticle {
        nodes {
          id
          url
          uid
          data {
            categories {
              category {
                document {
                  ... on PrismicCategory {
                    data {
                      title {
                        text
                      }
                    }
                    uid
                  }
                }
              }
            }
          }
        }
      }
      allPrismicCategory {
        nodes {
          uid
        }
      }
    }
  `, {}).then(result => {
    if (result.errors) {
      throw result.errors
    }

    // Create blog post pages.
    result.data.allPrismicRoadmapActivity.edges.forEach(edge => {
      createPage({
        // Path for this page — required
        path: `/roadmap/${edge.node.data.metric.document.uid}/${edge.node.uid}`,
        component: roadmapActivityTemplate,
        context: {
          id: edge.node.id,
          uid: edge.node.uid,
          metric_id: edge.node.data.metric.id,
        },
      })
    })

    const posts = result.data.allPrismicArticle.nodes
    const postPerPageNumber = 10;
    const pagesAmount = Math.ceil(posts.length / postPerPageNumber)

    Array.from({ length: pagesAmount }).forEach((item, index) => {
      createPage({
        path: index === 0 ? '/blog' : `/blog/page/${index + 1}`,
        component: path.resolve(__dirname, 'src/templates/blog-articles-list.js'),
        context: {
          limit: postPerPageNumber,
          skip: index * postPerPageNumber,
        },
      })
    })

    // Create article pages.
    posts.forEach(item => {
      const articleCategory = item.data.categories[0].category.document.data.title.text;

      // Necessary to send article category in snake case to component to compare with category slug
      const currentCategory = convertStringToSnakeCase(articleCategory);

      createPage({
        path: `/blog/${item.uid}`,
        component: path.resolve(__dirname, 'src/templates/article.js'),
        context: {
          currentCategory,
          uid: item.uid
        },
      })
    })

    // Create blog category pages.
    const categories = result.data.allPrismicCategory.nodes;
    const categoryPages = [];
    const categoryPostPerPageNumber = 5;

    categories.forEach(category => {
      categoryPages.push({
        category: category.uid,
        pages: null,
        posts: null
      })

      const categoryPosts = [];
      const pageCategory = categoryPages.find(element => element.category === category.uid);

      posts.forEach(post => {
        const postCategories = post.data.categories;

        postCategories.forEach(postCategory => {
          if (postCategory.category.document.uid === category.uid) {
            if (pageCategory) categoryPosts.push(post);
          };
        });
      })

      const categoryPagesAmount = Math.ceil(categoryPosts.length / categoryPostPerPageNumber);
  
      categoryPages[categoryPages.indexOf(pageCategory)].pages = categoryPagesAmount;
      categoryPages[categoryPages.indexOf(pageCategory)].posts = categoryPosts;
    })

    categoryPages.forEach(page => {
      Array.from({ length: page.pages }).forEach((entry, index) => {
        createPage({
          path: index === 0 ? `/blog/category/${page.category}` : `/blog/category/${page.category}/page/${index + 1}`,
          component: path.resolve(__dirname, 'src/templates/category.js'),
          context: {
            currentCategory: page.category,
            limit: categoryPostPerPageNumber,
            skip: index * categoryPostPerPageNumber,
          },
        })
      })
    })
  })
}
