require("dotenv").config({
  path: `.env.${process.env.NODE_ENV}`,
})

// SEO & Social Media header data
const website = require('./config/website')

module.exports = {
  siteMetadata: {
    url: website.url + website.pathPrefix,
    siteUrl: website.url + website.pathPrefix, // For gatsby-plugin-sitemap
    pathPrefix: website.pathPrefix,
    image: website.sharingImage,
    ogLanguage: website.ogLanguage,
    author: website.author,
    twitterUsername: website.twitterUsername,
    titleTemplate: website.titleTemplate,
    description: website.description,
    title: website.title,
  },
  plugins: [
    {
      resolve: "gatsby-plugin-react-svg",
      options: {
        rule: {
          include: /icons/,
          omitKeys: ['xmlnsDc', 'xmlnsCc', 'xmlnsRdf', 'xmlnsSvg', 'xmlnsSodipodi', 'xmlnsInkscape', 'style']
        }
      }
    },
    "gatsby-plugin-image",
    'gatsby-plugin-postcss',
    "gatsby-plugin-react-helmet",
    "gatsby-plugin-sitemap",
    {
      resolve: "gatsby-plugin-manifest",
      options: {
        icon: "src/images/icon.png",
      },
    },
    {
      resolve: `gatsby-plugin-sass`,
      options: {
        postCssPlugins: [
          require("tailwindcss"),
          require("./tailwind.config.js"), // Optional: Load custom Tailwind CSS configuration
        ],
      },
    },
    "gatsby-plugin-sharp",
    "gatsby-transformer-sharp",
    {
      resolve: "gatsby-source-filesystem",
      options: {
        name: "images",
        path: "./src/images/",
      },
      __key: "images",
    },
    {
      resolve: 'gatsby-source-prismic',
      options: {
        repositoryName: process.env.GATSBY_PRISMIC_REPO_NAME,
        accessToken: process.env.PRISMIC_ACCESS_TOKEN,
        customTypesApiToken: process.env.PRISMIC_CUSTOM_TYPES_API_TOKEN,
        linkResolver: require('./src/utils/LinkResolver').linkResolver,
      },
    },
    {
      resolve: 'gatsby-plugin-prismic-previews',
      options: {
        repositoryName: process.env.GATSBY_PRISMIC_REPO_NAME,
      },
    },
    {
      resolve: 'gatsby-plugin-google-fonts',
      options: {
        fonts: [`Roboto\:300,400,500,700`, `Roboto Condensed\:300,400,500,700`],
      },
      display: 'swap'
    },
    {
      resolve: "gatsby-plugin-hubspot",
      options: {
        trackingCode: "7577247",
        respectDNT: true,
        productionOnly: true,
      },
    },
    {
      resolve: 'gatsby-plugin-load-script',
      options: {
        src: 'https://widget.taggbox.com/embed.min.js',
      },
    },
    {
      resolve: 'gatsby-plugin-load-script',
      options: {
        src: 'https://cdn.addevent.com/libs/atc/1.6.1/atc.min.js',
      },
    },
    {
      resolve: `gatsby-transformer-remark`,
      options: {
        // Plugins configs
        plugins: [],
      },
    },
    {
      resolve: `gatsby-plugin-linkedin-insight`,
      options: {
        partnerId: `2137017`,
  
        // Include LinkedIn Insight in development.
        // Defaults to false meaning LinkedIn Insight will only be loaded in production.
        includeInDevelopment: false
      }
    },
    `gatsby-plugin-gatsby-cloud`,
    `gatsby-plugin-portal`,
    // Must be placed at the end
    'gatsby-plugin-offline',
  ],
};
