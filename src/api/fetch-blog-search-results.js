import * as prismic from '@prismicio/client';
import fetch from "node-fetch";

const routes = [
  {
    type: 'blog',
    path: '/:uid',
  },
];

const accessToken = process.env.PRISMIC_ACCESS_TOKEN;
const repoName = process.env.GATSBY_PRISMIC_REPO_NAME;
const endpoint = prismic.getEndpoint(repoName);
const client = prismic.createClient(endpoint, { routes, fetch, accessToken });

const fetchBlogSearchResults = async (params = {}) => {
  let results = [];

  const { terms, page = 1 } = params;

  if (!terms || typeof terms !== 'string') return results;

  results = await client.getByType('article', {
    graphQuery: `
      {
        article {
          author {
            full_name
            photo
          }
          categories {
            category {
              title
            }
          }
          excerpt
          main_image
          publication_time
          title
        }
      }
    `,
    orderings: {
      field: 'document.first_publication_date',
      direction: 'desc',
    },
    page,
    pageSize: 10,
    predicates: [prismic.predicate.fulltext('document', terms)]
  });

  return results;
};

export default fetchBlogSearchResults;
