import * as React from 'react'
import { graphql } from 'gatsby'
import { withPrismicPreview } from 'gatsby-plugin-prismic-previews'
import { RichText } from 'prismic-reactjs'

import Layout from '../../components/Layout'
import ResourceDetail from '../../components/ResourceDetail'
import SliceZone from '../../components/SliceZone'
import Seo from '../../components/Seo'

const ResourceTemplate = ({ data }) => {
  if (!data) return null
  const doc = data.prismicResource.data
  
  const {
    name,
    description,
  } = doc

  return (
    <Layout useDarkHeader={true} useDarkFooter={true}>
      <Seo 
        title={RichText.asText(name.richText)} 
        description={RichText.asText(description.richText)} 
      />
      
      <div>
        <ResourceDetail {...doc} />
        
        {doc.body ? <SliceZone sliceZone={doc.body} /> : null}
      </div>
    </Layout>
  )
}

export const query = graphql`
  query ResourceQuery($id: String) {
    prismicResource(id: { eq: $id }) {
      ...PrismicResourceFragment

      data {
        body {
          ...on PrismicSliceType {
            slice_type
          }
          ...ResourceDataBodyFeaturedTeamMembers
          ...ResourceDataBodyFullQuote
          ...ResourceDataBodyFeaturedSteeringGroups
          ...ResourceDataBodyFeaturedResources
          ...ResourceDataBodyFullWidthVideo
          ...ResourceDataBodyAboutSummaryBlock
          ...ResourceDataBodyHubspotFormWithTeamMember
          ...ResourceDataBodyTwoColumnImageChecklist
          ...ResourceDataBodyFeaturedOrgMembers
          ...ResourceDataBodyHubspotForm
          ...ResourceDataBodyToggleList
          ...ResourceDataBodyCallout
          ...ResourceDataBodyFeaturedEvents
          ...ResourceDataBodyContactTeamMembers
          ...ResourceDataBodySimpleRichText
          ...ResourceDataBodyLargeNewsletterForm
          ...ResourceDataBodyFeaturedTeamMembers
          ...ResourceDataBodyFeaturedIndividualMembers
          ...ResourceDataBodyCallToAction
        }
      }
    }
  }
`

export default withPrismicPreview(ResourceTemplate)
