import * as React from 'react'
import { withPrismicPreviewResolver } from 'gatsby-plugin-prismic-previews'
import { linkResolver } from '../utils/LinkResolver'

const BrandGuideline = () => {
  return (
    <div className="container-medium ">
      <div className="bg-gray-200 p-8">
        <h1>This is h1 headline</h1>
        <h2>This is h2 headline</h2>
        <h3>This is h3 headline</h3>
        <h4>This is h4 headline</h4>
        <h5>This is h5 headline</h5>
        <h6>This is h6 headline</h6>
        
        <h1 className="mt-12">The Roadmap to Sustainable Digital Infrastructure by 2030</h1>
      </div>

      <div className="bg-gray-200 mt-24 p-8">
        <p className="text-xs">The quick brown fox ...</p>
        <p className="text-sm">The quick brown fox ...</p>
        <p className="text-base">text-base should be 18px, Roboto, not bold</p>
        <p className="text-lg">text-lg should be 20px</p>
        <p className="text-xl">text-xl should be 24px</p>
        <p className="text-2xl">text-2xl should be 30px</p>
        <p className="text-3xl">text-3xl should be 36px</p>
        <p className="text-4xl">text-4xl should be 40px</p>
        <p className="text-5xl">text-5xl should be 54px</p>
        <p className="text-xl font-light font-display">This is a lead text example</p>
      </div>
    </div>
  )
}

export default withPrismicPreviewResolver(BrandGuideline, [
  {
    repositoryName: process.env.GATSBY_PRISMIC_REPO_NAME,
    linkResolver,
  },
])
