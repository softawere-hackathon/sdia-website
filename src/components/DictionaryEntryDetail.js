import * as React from 'react'
import { RichText } from 'prismic-reactjs'
import BackButton from './BackButton'
import { linkResolver } from '../utils/LinkResolver'

const DictionaryEntryDetail = ({ 
  explanation,
  word,
  image,
  last_publication_date,
}) => {
  
  return (
    <section className={`container-medium mt-20 sm:mt-32`}>
      <BackButton>Back to overview</BackButton>

      <div className="flex items-center text-gray-600 font-display text-lg mt-8">
        <p>
          Last updated: {last_publication_date}
        </p>
      </div>

      <h1 className="text-4-5xl mt-2 leading-snug">
        <span className="text-gray-600">Definition for </span>
        {RichText.asText(word.richText)}
      </h1>

      <div className="prose prose-xl font-display mt-2">
        <RichText render={explanation.richText} linkResolver={linkResolver} />
      </div>
    </section>
  )
}

export default DictionaryEntryDetail