import * as React from 'react'
import { RichText } from 'prismic-reactjs'
import BackButton from '../BackButton'
import { linkResolver } from '../../utils/LinkResolver'
import { filter, sortBy } from 'lodash'
import { format, parse } from 'date-fns'
import { Link } from 'gatsby'

import SdGoal from './SdGoal'
import ContributingMember from './ContributingMember'
import LinkButton from '../LinkButton'
import Challenge from './Challenge'

const supplyChainLayers = {
  'User': [],
  'Software Application': [],
  'Resource Provisioning': [],
  'Hardware': [],
  'Networks': [],
  'Data Center Facility': [],
  'Energy': [],
}

const criticalityOrder = {
  'Critical': 1,
  'High': 2,
  'Medium': 3,
  'Low': 4,
}

const ActivityDetail = ({ 
  id,
  impact_principle,
  name,
  target,
  start_date,
  end_date,
  members,
  sd_goals,
  metric,
  challenges,
}) => {
  const simpleChallenges = challenges.map((challenge) => (
    {
      id: challenge.id,
      uid: challenge.uid,
      sort_key: criticalityOrder[challenge.data.criticality],
      ...challenge.data,
    }
  ))
  const challengesByLayer = {}
  Object.keys(supplyChainLayers).forEach(layer => 
    challengesByLayer[layer] = sortBy(filter(simpleChallenges, ['supply_chain_layer', layer]), 'sort_key')
  )

  const start_date_obj = parse(start_date, 'yyyy-MM-dd', new Date())
  const end_date_obj = parse(end_date, 'yyyy-MM-dd', new Date())

  console.log(metric)

  return (
    <>
      <section className={`container mt-20 sm:mt-32`}>
        <div className="flex flex-wrap -m-4">
          <div className="p-4 md:w-2/3">
            <BackButton>Back to roadmap</BackButton>

            <div className="flex items-center text-gray-600 font-display text-lg mt-8">
              <p>
                Impact Category: <strong>{metric.name}</strong>
              </p>
            </div>

            <h1 className="text-4-5xl mt-2 leading-snug title-font">
              <span className="text-gray-600">Priority: </span>
              {RichText.asText(name.richText)}
            </h1>

            <div className="prose prose-xl font-display mt-2">
              <RichText render={impact_principle.richText} linkResolver={linkResolver} />
              <RichText render={target.richText} linkResolver={linkResolver} />
              <p>
                <strong>Estimated timeframe:&nbsp;</strong>
                {format(start_date_obj, 'MMMM yyyy')} to {format(end_date_obj, 'MMMM yyyy')}
              </p>
            </div>
          </div>

          {Array.isArray(members) && members.length ? 
            <div className="p-4 md:pl-8 md:w-1/3">
              <h3 className="mb-6 text-xl md:mt-28 lg:text-2xl font-medium title-font">
                Work on this priority is sponsored by:
              </h3>
              {members.map((item, index) => {
                if(item && item.member && item.member.document) {
                  return ( 
                    <ContributingMember 
                      key={`activity-sponsor-${id}-${index}`}
                      {...item.member.document.data} 
                    />
                  )
                } else {
                  return null
                }
              })}
            </div>
          : 
            <div className="p-4 md:pl-8 md:w-1/3">
              <h3 className="mb-6 text-xl md:mt-28 lg:text-2xl font-medium title-font">
                Would you like to sponsor this priority?
              </h3>
              <LinkButton link={{ url: `/memberships/sponsor-membership`, link_type: 'Document' }} isWarning>
                Let us know.
              </LinkButton>
            </div>
          }
        </div>
      </section>

      {sd_goals && sd_goals.length > 0 ? 
        <section className="body-font container">
          <div className="container px-5 pt-24 pb-12 mx-auto">
            
            <div className="flex flex-col text-center w-full mb-12">
              <h5 className="text-sm text-gray-600 uppercase tracking-wide mb-1">
                United Nations - Sustainable Development Goals
              </h5>
              <h4 className="sm:text-3xl text-2xl">This priority contributes to global sustainability goals:</h4>
            </div>

            <div className="flex flex-wrap -m-4 text-gray-600">    
              {sd_goals.map((goal, index) => (
                <div className="p-4 md:w-1/3">
                  <div className="flex rounded-lg h-full bg-gray-100 p-8 flex-col">
                    <div className="flex items-center mb-3">
                    <SdGoal 
                        {...goal.sd_goal.document.data} 
                        key={`sd-goal-${index}`}
                      />
                    </div>
                    <div className="flex-grow">
                      <p className="leading-relaxed text-base">
                        {RichText.asText(goal.sd_goal.document.data.name.richText)}
                      </p>
                      <a 
                        className="mt-3 text-secondary-700 inline-flex items-center" 
                        href={`https://sdgs.un.org/goals/goal${goal.sd_goal.document.data.goal_number}`}
                        target="_blank"
                        rel="noreferrer"
                      >
                        Learn More
                        <svg fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" className="w-4 h-4 ml-2" viewBox="0 0 24 24">
                          <path d="M5 12h14M12 5l7 7-7 7"></path>
                        </svg>
                      </a>
                    </div>
                  </div>
                </div>
              ))}
            </div>
          </div>
        </section>
      : null}

      <section className="body-font">
        <div className="container px-5 py-12 mx-auto">
          <div className="flex flex-wrap w-full mb-12 flex-col items-center text-center">
            <h4 className="sm:text-3xl text-2xl">Identified challenges</h4>
            <p className="lg:w-2/3 w-full text-xl font-display text-gray-500 mt-3 text-center">
              These are the challenges and barriers identified to reach the target of this roadmap priority. They are grouped by supply chain layer based on our digital economy taxonomy.
            </p>
          </div>
          
          {Object.keys(challengesByLayer).map((layer) => (
            <div className="first:mt-0 mt-12">    
              <h5 className="mb-4 underline">{layer}</h5>
              {challengesByLayer[layer].length > 0 ?
                (<div className="flex flex-wrap -m-4 text-gray-600">
                  {challengesByLayer[layer].map((challenge) => <Challenge {...challenge} key={challenge.id} />)}
                </div>) :
                (<div className="-m-4 text-gray-600 my-12">
                  <p className='text-center'>
                    We have not identified a challenge in this later yet. Would you like to <Link className='text-secondary-800 underline hover:no-underline' to={`/submit-roadmap-challenge`}>submit one?</Link>
                  </p>
                </div>)
              }
            </div>
          ))}

          <section className='flex flex-col items-center mx-auto mt-16'>
            <h6 className='mb-4 w-full text-center'>Have we missed something?</h6>

            <LinkButton 
              isWarning 
              className={`text-lg`} 
              link={{ url: `/submit-roadmap-challenge`, link_type: 'Document' }}
            >
              Submit a challenge
            </LinkButton>
          </section>
          
        </div>
      </section>
    </>
  )
}

export default ActivityDetail