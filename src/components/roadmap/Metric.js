import { graphql } from 'gatsby'

export const roadmapMetricFragment = graphql`
  fragment PrismicRoadmapMetricFragment on PrismicRoadmapMetric {
    _previewable
    url
    uid
    last_publication_date(fromNow: true)
    id

    data {
      current_measure
      name
      explanation {
        richText
      }
      activities {
        activity {
          document {
            ...PrismicRoadmapActivityFragment
          }
        }
      }
    }
  }
`