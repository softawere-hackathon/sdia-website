import * as React from 'react'
import { RichText } from 'prismic-reactjs'
import { linkResolver } from '../utils/LinkResolver'
import LinkButton from './LinkButton'
import { GatsbyImage, getImage } from 'gatsby-plugin-image'

const ResourceDetail = ({ 
  url,
  preview_image,
  link_to_member_content,
  description,
  members_only,
  published_on,
  resource,
  type,
  name,
  detail_cta,
  detail_cta_label,
}) => {
  // const start_date_obj = parseISO(start_date)
  // const end_date_obj = parseISO(end_date)
  const image = getImage(preview_image)
  
  return (
    <section class="bg-primary-100 py-24 md:py-24 lg:pt-36">
      <div class="mx-auto max-w-4xl px-4 md:px-8">
        <div class="grid md:grid-cols-5 gap-4 md:gap-12 relative">
          <div className="md:col-span-2">
            {/* <BackButton>
              Back to overview
            </BackButton> */}

            <GatsbyImage
              image={image}
              className="shadow-md rounded"
              alt={RichText.asText(name.richText)}
            />
          </div>

          <div class="mt-12 md:mt-0 md:col-span-3">
            <div>
              <p class="uppercase tracking-wide text-secondary-900">
                {type}
              </p>
            </div>
            <h2 class="text-4xl font-display mt-1 font-bold">
              {RichText.asText(name.richText)}
            </h2>
            
            <span class="h-1 w-12 mt-4 bg-green-900 inline-block"></span>

            <div class="prose text-lg font-display mt-4 text-gray-900 leading-relaxed">
              <RichText render={description.richText} linkResolver={linkResolver} />
            </div>

            <div className="">
              <LinkButton 
                className="mt-6 inline-block antialiased c-button--primary-dark"
                label={detail_cta_label}
                link={detail_cta}
              />
            </div>
          </div>
        </div>
      </div>
    </section>
  )
}

export default ResourceDetail
