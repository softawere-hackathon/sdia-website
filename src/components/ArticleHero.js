import * as React from 'react';
import PropTypes from 'prop-types';
import { RichText } from 'prismic-reactjs';
import { GatsbyImage, getImage } from "gatsby-plugin-image";

import ArticleSocialShare from './ArticleSocialShare';


const ArticleHero = ({ author, categories, mainImage, publicationTime, shareData, title }) => {
  if (!title && !mainImage) return null;

  const authorImage = getImage(author.document?.data?.photo);
  const authorName = author.document?.data?.full_name?.text;
  const date = new Date(publicationTime);
  const formattedDate = new Intl.DateTimeFormat('en-GB', { dateStyle: 'medium' }).format(date);
  const heroImage = getImage(mainImage);

  return (
    <section className="max-w-[1248px]">
      {!!mainImage && (
        <GatsbyImage
          image={heroImage}
          className="flex h-[380px] w-full rounded-[8px]"
          alt={heroImage.alt || RichText.asText(title)}
        />
      )}
      <div className="container">
        <div className="lg:max-w-[872px] xl:max-w-[920px] ml-auto mr-auto mt-[-120px] relative flex flex-col gap-[18px] shadow-xl shadow-[#0000000D] rounded-xl bg-white p-[20px] sm:p-[32px] z-10">
          {((Array.isArray(categories) && !!categories.length) || !!formattedDate) && (
            <div className="flex items-center justify-between">
              {Array.isArray(categories) && !!categories.length && (
                <div className="rounded-[30px] py-[4px] px-[8px] bg-[#38604B1F] text-[14px] text-[#1D4732] leading-[14px]">
                  {categories[0]?.category?.document?.data?.title?.text}
                </div>
              )}
              {!!formattedDate && (
                <span className="text-[14px] leading-[16px] text-[#5C6372]">
                  {formattedDate}
                </span>
              )}
            </div>
          )}
          {(!!title) && (
            <h1 className="text-[40px] leading-[50px]">
              {RichText.asText(title)}
            </h1>
          )}
          <div className="flex justify-between">
            {(!!authorImage || !!authorName) && (
              <div className="flex items-center">
                {!!authorImage && (
                  <div className="rounded-full h-[32px] w-[32px]">
                    <GatsbyImage
                      alt={mainImage.alt || authorName || ''}
                      className="rounded-full h-full w-full"
                      image={authorImage}
                      objectFit="cover"
                      objectPosition="top"
                    />
                  </div>
                )}
                {!!authorName && (
                  <div className="text-[16px] leading-[19px] text-[#1B202B] ml-[12px]">
                    {authorName}
                  </div>
                )}
              </div>
            )}
            <ArticleSocialShare
              className="lg:hidden"
              url={shareData.url}
            />
          </div>
        </div>
      </div>
    </section>
  );
};

ArticleHero.propTypes = {
  author: PropTypes.shape({
    document: PropTypes.shape({
      data: PropTypes.shape({
        full_name: PropTypes.shape({
          text: PropTypes.string
        }),
        photo: PropTypes.shape({
          alt: PropTypes.string,
          gatsbyImageData: PropTypes.shape({
            height: PropTypes.number,
            layout: PropTypes.string,
            width: PropTypes.number
          })
        })
      })
    })
  }),
  categories: PropTypes.arrayOf(PropTypes.shape({
    document: PropTypes.shape({
      data: PropTypes.shape({
        title: PropTypes.shape({
          text: PropTypes.string
        })
      })
    })
  })),
  mainImage: PropTypes.shape({
    alt: PropTypes.string
  }),
  publicationTime: PropTypes.string,
  text: PropTypes.arrayOf(PropTypes.shape({
    text: PropTypes.string,
    type: PropTypes.string
  })),
  title: PropTypes.arrayOf(PropTypes.shape({
    text: PropTypes.string,
    type: PropTypes.string
  })),
};

ArticleHero.defaultProps = {
  categories: null,
  mainImage: null,
  publicationTime: null,
  text: null,
  title: null
};

export default ArticleHero;
