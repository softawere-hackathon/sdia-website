import { graphql } from 'gatsby'

export const eventFragment = graphql`
  fragment PrismicEventFragment on PrismicEvent {
    _previewable
    uid
    url
    data {
      timezone
      stream_link {
        url
        type
      }
      speakers: speakers_list {
        linkedin_profile {
          url
        }
        twitter_link {
          url
        }
        biography {
          richText
        }
        full_name {
          richText
        }
        photo {
          gatsbyImageData(
            width: 130
            height: 130
            imgixParams: {fit: "facearea", facepad: 3.5, maxH: 130}
          )
        }
        title: speaker_title {
          richText
        }
      }

      upcoming_event
      description {
        richText
      }
      end_date
      free_for_members
      location {
        richText
      }
      image {
        gatsbyImageData(width: 300)
        url
      }
      main_link {
        url
        uid
        type
        target
        link_type
      }
      main_link_label
      presence
      type
      title {
        richText
      }
      tickets_link_label
      tickets_link {
        url
        uid
        type
        target
        link_type
      }
      ticket_information {
        richText
      }
      ticket_costs {
        richText
      }
      start_date
      sessions {
        timestamp
        hosted_by {
          richText
        }
        hosted_by_the_sdia
        session_description {
          richText
        }
        session_link {
          url
          uid
          type
          target
          link_type
        }
        session_link_label
        session_title {
          richText
        }
        team_member {
          document {
            ... on PrismicTeamMember {
              id
              data {
                full_name {
                  richText
                }
                linkedin_profile {
                  url
                  target
                  type
                  uid
                  link_type
                }
                photo {
                  gatsbyImageData(width: 200)
                }
                title {
                  richText
                }
              }
            }
          }
        }
      }
      requires_tickets
      recordings {
        # recording_embed {
        #   author_name
        #   author_url
        #   embed_url
        #   height
        #   html
        #   id
        #   prismicId
        #   provider_name
        #   provider_url
        #   thumbnail_height
        #   thumbnail_url
        #   thumbnail_width
        #   title
        #   type
        #   width
        # }
        recording_preview {
          url
          gatsbyImageData(width: 200)
        }
      }
      press_coverage {
        coverage_title {
          richText
        }
        coverage_preview {
          gatsbyImageData(width: 200)
          alt
        }
        coverage_media {
          richText
        }
        coverage_link {
          url
          uid
          type
          target
          link_type
        }
        coverage_link_label
        coverage_excerpt {
          richText
        }
      }
      presentations {
        presentation_preview {
          gatsbyImageData(width: 200)
          alt
        }
        # presentation_embed {
        #   author_name
        #   author_url
        #   embed_url
        #   height
        #   html
        #   id
        #   thumbnail_width
        #   thumbnail_url
        #   provider_url
        #   thumbnail_height
        #   provider_name
        #   prismicId
        #   title
        #   type
        #   width
        #   version
        # }
        presentation_document {
          url
          uid
          type
          target
          link_type
        }
      }
    }
  }
`