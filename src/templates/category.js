import * as React from 'react';
import { Link, graphql } from 'gatsby';
import { withPrismicPreview } from 'gatsby-plugin-prismic-previews';

import ArticlesList from '../components/ArticlesList';
import BlogCategories from '../components/BlogCategories';
import Layout from '../components/Layout';
import Pagination from '../components/Pagination';
import Seo from '../components/Seo';

// Assets
import BackToNewsButtonArrow from '../images/icons/arrow-left.svg';

const CategoryTemplate = ({ data }) => {
  if (!data) return null;

  const allArticles = data.articles.nodes;
  const categories = data.allCategories.nodes;
  const currentCategoryName = data.currentCategory.nodes[0].data.title.text;
  const currentCategorySlug = data.currentCategory.nodes[0].uid;
  const { pageInfo } = data.articles;

  return (
    <Layout
      hideHeaderBorderTop
      useDarkHeader
      useDarkFooter
    >
      <Seo title={currentCategoryName} description={currentCategoryName} />

      <div className="container max-w-[1248px] mt-[111px]">
        <Link
          to="/blog"
          className="cursor-pointer inline-flex items-center mb-[32px]"
        >
          <BackToNewsButtonArrow className="flex" />
          <span className="ml-[20px] text-[#1D4732]">
            Back to News
          </span>
        </Link>
        <section>
          <h2 className="text-[32px] leading-[50px] text-[#1D4732]">
            {currentCategoryName}
          </h2>
          <div className="grid md:grid-cols-[1fr_239px] gap-[40px] md:gap-[73px] mt-[32px]">
            <main>
              <ArticlesList articles={allArticles} />
            </main>
            <aside>
              <BlogCategories
                currentCategory={currentCategoryName}
                categories={categories}
              />
            </aside>
          </div>
          {!!pageInfo && pageInfo.pageCount > 1 && (
            <div className="flex justify-center mt-12 md:mt-[80px]">
              <Pagination
                basePath={`/blog/category/${currentCategorySlug}`}
                pageInfo={pageInfo}
                pagePath={`/blog/category/${currentCategorySlug}/page`}
              />
            </div>
          )}
        </section>
      </div>
    </Layout>
  );
}

export const query = graphql`
  query CategoryArticlesListQuery($currentCategory: String, $limit: Int!, $skip: Int!) {
    articles: allPrismicArticle(
      filter: {
        data: {
          categories: {
            elemMatch: {
              category: {
                slug: {
                  eq: $currentCategory
                }
              }
            }
          }
        }
      }
      sort: { fields: data___date, order: DESC }
      limit: $limit
      skip: $skip
    ) {
      nodes {
        ...PrismicArticleFragment
        uid
      }
      pageInfo {
        currentPage
        pageCount
        hasNextPage
        hasPreviousPage
      }
    }
    allCategories: allPrismicCategory {
      nodes {
        data {
          title {
            text
          }
        }
        id
        uid
      }
    }
    currentCategory: allPrismicCategory(
      filter: {
        uid: {
          eq: $currentCategory
        }
      }
    ) {
      nodes {
        data {
          title {
            text
          }
        }
        uid
      }
    }
  }
`

export default withPrismicPreview(CategoryTemplate);
