import * as React from 'react';
import { graphql, Link } from 'gatsby';
import { withPrismicPreview } from 'gatsby-plugin-prismic-previews';
import { RichText } from 'prismic-reactjs';

import ArticleHero from '../components/ArticleHero';
import ArticleNewsletter from '../components/ArticleNewsletter';
import ArticleRelatedNews from '../components/ArticleRelatedNews';
import ArticleSocialShare from '../components/ArticleSocialShare';
import Layout from '../components/Layout';
import Seo from '../components/Seo';
import SliceZone from '../components/SliceZone';

// Assets
import BackToNewsButtonArrow from '../images/icons/arrow-left.svg';


const ArticleTamplate = ({ data }) => {
  if (!data) return null;

  const articleData = data.individualArticle.nodes[0].data;
  const sameCategoryArticles = data.relatedArticles.nodes;

  const relatedArticles = sameCategoryArticles.filter(article => article.uid !== data.individualArticle.nodes[0].uid);

  const {
    author,
    categories,
    download_file,
    main_image,
    publication_time,
    title
  } = articleData;

  return (
    <Layout
      hideHeaderBorderTop
      useDarkHeader
      useDarkFooter
    >
      <Seo
        article
        description={RichText.asText(title.richText)}
        title={RichText.asText(title.richText)}
      />

      <div className="container max-w-[1248px] mt-[111px]">
        <Link
          to="/blog"
          className="cursor-pointer inline-flex items-center mb-[32px]"
        >
          <BackToNewsButtonArrow className="flex" />
          <span className="ml-[20px] text-[#1D4732]">
            Back to News
          </span>
        </Link>
        <ArticleHero
          author={author}
          categories={categories}
          mainImage={main_image}
          publicationTime={publication_time}
          shareData={{
            url: `https://sdialliance.org/blog/${data.individualArticle.nodes[0].uid}`
          }}
          title={title.richText}
        />
      </div>
      <div className="container relative max-w-[1248px]">
        <div className="hidden lg:block lg:sticky lg:top-[50px]">
          <ArticleSocialShare
            className="lg:absolute top-0 left-0"
            url={`https://sdialliance.org/blog/${data.individualArticle.nodes[0].uid}`}
          />
        </div>
        <SliceZone
          className="container lg:max-w-[920px] xl:max-w-[970px] mt-[50px]"
          sliceZone={articleData.body}
        />
      </div>
      {!!download_file && download_file.url && (
        <div className="container flex justify-center mt-[40px]">
          <a
            className="text-[16px] leading-[20px] text-white bg-[#E69635] py-[10px] px-[20px] rounded-[5px]"
            href={download_file.url}
            target="_blank"
          >
            Download Press Release
          </a>
        </div>
      )}
      <ArticleNewsletter />
      <div className="container max-w-[1248px] my-[120px]">
        <ArticleRelatedNews items={relatedArticles} />
      </div>
    </Layout>
  );
}

export const query = graphql`
  query ArticleQuery($uid: String $currentCategory: String) {
    relatedArticles: allPrismicArticle(
      filter: {
        data: {
          categories: {
            elemMatch: {
              category: {
                slug: {
                  eq: $currentCategory
                }
              }
            }
          }
        }
      }
    ) {
      nodes {
        ...PrismicArticleFragment
        uid
      }
    }
    individualArticle: allPrismicArticle(
      filter: {
        uid: {
          eq: $uid
        }
      }
    ) {
      nodes {
        ...PrismicArticleFragment
      }
    }
  }
`

export default withPrismicPreview(ArticleTamplate);
