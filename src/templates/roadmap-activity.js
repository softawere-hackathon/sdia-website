import * as React from 'react'
import { graphql } from 'gatsby'
import { withPrismicPreview } from 'gatsby-plugin-prismic-previews'
import { RichText } from 'prismic-reactjs'

import Layout from '../components/Layout'
import ActivityDetail from '../components/roadmap/ActivityDetail'
import Seo from '../components/Seo'

const RoadmapActivityTemplate = ({ data }) => {
  if (!data) return null
  const doc = data.prismicRoadmapActivity
  const challengeDoc = data.allPrismicRoadmapChallenge
  const challenges = challengeDoc.edges.map(item => item.node)
  console.log(doc.data)
  
  const {
    name,
    impact_principle,
    metric,
  } = doc.data

  return (
    <Layout useDarkHeader={true} useDarkFooter={true}>
      <Seo title={RichText.asText(name.richText)} description={RichText.asText(impact_principle.richText)} />
      
      <div className="pt-12 md:pt-6">
        <ActivityDetail 
          challenges={challenges}
          {...doc.data} 
          {...doc} 
          metric={metric.document.data} 
        />
        
        {/* {doc.body ? <SliceZone sliceZone={doc.body} /> : null} */}
      </div>
    </Layout>
  )
}

export const query = graphql`
  query RoadmapActivityQuery($id: String, $uid: String) {
    prismicRoadmapActivity(id: { eq: $id }) {
      ...PrismicRoadmapActivityFragment
    }

    allPrismicRoadmapChallenge(
      filter: {data: {related_activity: {uid: {eq: $uid }}}}
    ) {
      edges {
        node {
          ...PrismicRoadmapChallengeFragment
        }
      }
    }
  }
`

export default withPrismicPreview(RoadmapActivityTemplate)
