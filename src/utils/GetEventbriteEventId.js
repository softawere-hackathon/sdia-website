const getEventbriteEventId = (eventUrl) => {
  if (typeof eventUrl !== 'string') return null;

  return eventUrl.substring(eventUrl.lastIndexOf('-') + 1, eventUrl.length);
}

export default getEventbriteEventId;
